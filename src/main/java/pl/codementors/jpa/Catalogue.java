package pl.codementors.jpa;

import pl.codementors.jpa.Models.Cave;
import pl.codementors.jpa.Models.Cave_;
import pl.codementors.jpa.Models.Goblin;
import pl.codementors.jpa.Models.Goblin_;
import pl.codementors.jpa.Models.Weapon;
import pl.codementors.jpa.Models.Weapon_;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

import static pl.codementors.jpa.Models.Weapon_.goblin;

public class Catalogue {
    private GoblinDao goblinDao;
    private WeaponDao weaponDao;
    private CaveDao caveDao;

    public Catalogue(GoblinDao goblinDao, WeaponDao weaponDao, CaveDao caveDao) {
        this.goblinDao = goblinDao;
        this.weaponDao = weaponDao;
        this.caveDao = caveDao;
    }

    public void addCave(int area) {
        Cave cave = new Cave(area);
        caveDao.persist(cave);
    }

    public void addGoblin(String name, int height, int caveId) {
        Goblin goblin = new Goblin(name, height, caveDao.find(caveId));
        goblinDao.persist(goblin);
    }

    public void addWeapon(String name, int damage, Weapon.Weapon_type weapon_type, int goblinId) {
        Weapon weapon = new Weapon(name, damage, weapon_type);
        weapon.setGoblin(goblinDao.find(goblinId));
        weaponDao.persist(weapon);
    }

    public void deleteCave(int caveId){
        caveDao.delete(caveDao.find(caveId));
    }

    public void deleteGoblin(int goblinId){
        goblinDao.delete(goblinDao.find(goblinId));
    }

    public void deleteWeapon(int weaponId){
        weaponDao.delete(weaponDao.find(weaponId));
    }

    public List<Goblin> writeAllArmedGoblins() {
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Goblin> query = cb.createQuery(Goblin.class);
        Root<Goblin> root = query.from(Goblin.class);
        query.where(root.get(Goblin_.weapon).get(goblin).isNotNull());
        return em.createQuery(query).getResultList();
    }

    public List<Goblin> writeAllUnarmedGoblins() {
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Goblin> query = cb.createQuery(Goblin.class);
        Root<Goblin> root = query.from(Goblin.class);
        Join<Goblin, Weapon> join = root.join(Goblin_.weapon, JoinType.LEFT);
        query.where(cb.isNull(join.get(Weapon_.goblin)));
        return em.createQuery(query).getResultList();
    }

    public List<Goblin> writeGoblinsHigherThan(int height) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Goblin> query = cb.createQuery(Goblin.class);
        Root<Goblin> root = query.from(Goblin.class);
        query.where(cb.gt(root.get(Goblin_.height), height));
        return em.createQuery(query).getResultList();
    }

    public List<Weapon> writeAllWeaponsWithoutOwner(){
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Weapon> query = cb.createQuery(Weapon.class);
        Root<Weapon> root = query.from(Weapon.class);
        query.where(root.get(Weapon_.goblin).isNull());
        return em.createQuery(query).getResultList();
    }

    public List<Cave> writeEmptyCaves(){
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Cave> query = cb.createQuery(Cave.class);
        Root<Cave> root = query.from(Cave.class);
        query.where(cb.isEmpty(root.get(Cave_.goblins)));
        return em.createQuery(query).getResultList();
    }

    public void writeObjectWithStorage(String objectName) {
        if (objectName.equalsIgnoreCase("cave")) {
            System.out.println(caveDao.findAll());
        }
        if (objectName.equalsIgnoreCase("goblin")) {
            System.out.println(goblinDao.findAll());
        }
        if (objectName.equalsIgnoreCase("weapon")) {
            System.out.println(weaponDao.findAll());
        }
    }
}
