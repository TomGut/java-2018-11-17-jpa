package pl.codementors.jpa.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "caves")
public class Cave {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int area;

    @OneToMany(mappedBy = "cave", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Goblin> goblins = new ArrayList<>();

    public Cave(){
    }

    public Cave(int area) {
        this.area = area;
    }

    public List<Goblin> getGoblins() {
        return goblins;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "\nCave:"
                + "id="
                + id
                + ", area="
                + area
                + ", goblins inside="
                + this
                .getGoblins()
                .size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cave cave = (Cave) o;

        if (id != cave.id) return false;
        if (area != cave.area) return false;
        return goblins != null ? goblins.equals(cave.goblins) : cave.goblins == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + area;
        result = 31 * result + (goblins != null ? goblins.hashCode() : 0);
        return result;
    }
}
