package pl.codementors.jpa.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "weapons")
public class Weapon implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private int damage;

    public enum Weapon_type {
        PUNCTURE,
        HACKED,
        BLUNT;
    }

    @Enumerated(EnumType.STRING)
    @Column
    private Weapon_type weapon_type;

    @OneToOne
    @JoinColumn(referencedColumnName = "id")
    private Goblin goblin;

    public Weapon(String name, int damage, Weapon_type weapon_type) {
        this.name = name;
        this.damage = damage;
        this.weapon_type = weapon_type;
    }

    public Weapon(){
    }

    public Goblin getGoblin() {
        return goblin;
    }

    public void setGoblin(Goblin goblin) {
        this.goblin = goblin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Weapon_type getWeapon_type() {
        return weapon_type;
    }

    public void setWeapon_type(Weapon_type weapon_type) {
        this.weapon_type = weapon_type;
    }

    @Override
    public String toString() {
        return "\nWeapon:"
                + "id="
                + id
                + ", name='"
                + name
                + '\''
                + ", damage="
                + damage
                + ", weapon_type="
                + weapon_type
                + ", goblin="
                + (goblin !=null ? goblin.getName() : null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Weapon weapon = (Weapon) o;

        if (id != weapon.id) return false;
        if (damage != weapon.damage) return false;
        if (name != null ? !name.equals(weapon.name) : weapon.name != null) return false;
        if (weapon_type != weapon.weapon_type) return false;
        return goblin != null ? goblin.equals(weapon.goblin) : weapon.goblin == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + damage;
        result = 31 * result + (weapon_type != null ? weapon_type.hashCode() : 0);
        result = 31 * result + (goblin != null ? goblin.hashCode() : 0);
        return result;
    }
}
