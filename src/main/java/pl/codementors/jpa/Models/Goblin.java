package pl.codementors.jpa.Models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "goblins")
public class Goblin implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private int height;

    @OneToOne(mappedBy = "goblin", cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "name")
    private Weapon weapon;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Cave cave;

    public Goblin(){
    }

    public Goblin(String name, int height, Cave cave) {
        this.cave = cave;
        this.name = name;
        this.height = height;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Cave getCave() {
        return cave;
    }

    public void setCave(Cave cave) {
        this.cave = cave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "\nGoblin:"
                + "id="
                + id
                + " ,name="
                + name
                + " ,height="
                + height
                + " ,weapon="
                + (weapon !=null ? weapon.getName() : null)
                + " ,cave="
                + (cave !=null ? cave.getId() : null);
    }
}
