package pl.codementors.jpa;

import pl.codementors.jpa.Models.Weapon;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class WeaponDao {

    public Weapon persist(Weapon weapon) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(weapon);
        tx.commit();
        em.close();
        return weapon;
    }

    public Weapon merge(Weapon weapon) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        weapon = em.merge(weapon);
        tx.commit();
        em.close();
        return weapon;
    }

    public Weapon find(int id) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        Weapon weapon = em.find(Weapon.class, id);
        em.close();
        return weapon;
    }

    public void delete(Weapon weapon) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(weapon));
        tx.commit();
        em.close();
    }

    public List<Weapon> findAll() {
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Weapon> query = cb.createQuery(Weapon.class);
        query.from(Weapon.class);
        List<Weapon> weapons = em.createQuery(query).getResultList();
        em.close();
        return weapons;
    }


}
