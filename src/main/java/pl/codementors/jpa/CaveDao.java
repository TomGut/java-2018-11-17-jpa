package pl.codementors.jpa;

import pl.codementors.jpa.Models.Cave;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class CaveDao {

    public Cave persist(Cave cave) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(cave);
        tx.commit();
        em.close();
        return cave;
    }

    public Cave merge(Cave cave) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        cave = em.merge(cave);
        tx.commit();
        em.close();
        return cave;
    }

    public Cave find(int id) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        Cave cave = em.find(Cave.class, id);
        em.close();
        return cave;
    }

    public void delete(Cave cave) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(cave));
        tx.commit();
        em.close();
    }

    public List<Cave> findAll() {
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();//Builder for creating a query.
        CriteriaQuery<Cave> query = cb.createQuery(Cave.class);//Query object declaring query return object.
        query.from(Cave.class);//From which entity (table) data will be fetched.
        List<Cave> caves = em.createQuery(query).getResultList();
        em.close();
        return caves;
    }
}
