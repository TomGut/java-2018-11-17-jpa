package pl.codementors.jpa;

import pl.codementors.jpa.Models.Weapon;

public class Main {
    public static void main(String[] args) {
        CaveDao caveDao = new CaveDao();
        GoblinDao goblinDao = new GoblinDao();
        WeaponDao weaponDao = new WeaponDao();
        Catalogue catalogueDao = new Catalogue(goblinDao, weaponDao, caveDao);

        catalogueDao.addCave(100);
        catalogueDao.addCave(200);
        catalogueDao.addCave(300);

        catalogueDao.addGoblin("Goblin1", 100, 1);
        catalogueDao.addGoblin("Goblin2", 110, 1);
        catalogueDao.addGoblin("Goblin3", 120, 1);
        catalogueDao.addGoblin("Goblin4", 130, 2);
        catalogueDao.addGoblin("Goblin5", 140, 2);
        catalogueDao.addGoblin("Goblin6", 150, 2);
        catalogueDao.addGoblin("Goblin7", 160, 0);
        catalogueDao.addGoblin("Goblin8", 170, 0);

        catalogueDao.addWeapon("Miecz1", 100, Weapon.Weapon_type.PUNCTURE, 1);
        catalogueDao.addWeapon("Miecz2", 110, Weapon.Weapon_type.PUNCTURE, 2);
        catalogueDao.addWeapon("Miecz3", 120, Weapon.Weapon_type.PUNCTURE, 3);
        catalogueDao.addWeapon("Miecz4", 130, Weapon.Weapon_type.PUNCTURE, 4);
        catalogueDao.addWeapon("Miecz5", 140, Weapon.Weapon_type.PUNCTURE, 5);
        catalogueDao.addWeapon("Miecz6", 150, Weapon.Weapon_type.PUNCTURE, 6);
        catalogueDao.addWeapon("Miecz7", 160, Weapon.Weapon_type.PUNCTURE, 0);
        catalogueDao.addWeapon("Miecz8", 170, Weapon.Weapon_type.PUNCTURE, 0);

        System.out.println("Caves");
        catalogueDao.writeObjectWithStorage("cave");
        System.out.println("Goblins");
        catalogueDao.writeObjectWithStorage("goblin");
        System.out.println("Weapons");
        catalogueDao.writeObjectWithStorage("weapon");
        System.out.println("All armed goblins");
        System.out.println(catalogueDao.writeAllArmedGoblins());
        System.out.println("All unarmed goblins");
        System.out.println(catalogueDao.writeAllUnarmedGoblins());
        System.out.println("Empty caves");
        System.out.println(catalogueDao.writeEmptyCaves());
        System.out.println("Height greater than 130");
        System.out.println(catalogueDao.writeGoblinsHigherThan(130));
        System.out.println("Weapons without owners");
        System.out.println(catalogueDao.writeAllWeaponsWithoutOwner());

        System.out.println("Usuwanie");

        catalogueDao.deleteWeapon(1);
        catalogueDao.deleteWeapon(2);
        catalogueDao.deleteWeapon(3);
        catalogueDao.deleteWeapon(4);
        catalogueDao.deleteWeapon(5);
        catalogueDao.deleteWeapon(6);
        catalogueDao.deleteWeapon(7);
        catalogueDao.deleteWeapon(8);
        System.out.println("Po usunięciu broni");
        catalogueDao.writeObjectWithStorage("cave");
        catalogueDao.writeObjectWithStorage("goblin");
        catalogueDao.writeObjectWithStorage("weapon");

        catalogueDao.deleteGoblin(1);
        catalogueDao.deleteGoblin(2);
        catalogueDao.deleteGoblin(3);
        catalogueDao.deleteGoblin(4);
        catalogueDao.deleteGoblin(5);
        catalogueDao.deleteGoblin(6);
        catalogueDao.deleteGoblin(7);
        catalogueDao.deleteGoblin(8);
        System.out.println("Po usunięciu goblinów");
        catalogueDao.writeObjectWithStorage("cave");
        catalogueDao.writeObjectWithStorage("goblin");
        catalogueDao.writeObjectWithStorage("weapon");

        catalogueDao.deleteCave(1);
        catalogueDao.deleteCave(2);
        catalogueDao.deleteCave(3);
        System.out.println("Po usunięciu cave");
        catalogueDao.writeObjectWithStorage("cave");
        catalogueDao.writeObjectWithStorage("goblin");
        catalogueDao.writeObjectWithStorage("weapon");

        EntityFactoryManager.close();
    }
}
