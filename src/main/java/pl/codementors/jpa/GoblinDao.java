package pl.codementors.jpa;

import pl.codementors.jpa.Models.Goblin;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class GoblinDao {
    public Goblin persist(Goblin goblin) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(goblin);
        tx.commit();
        em.close();
        return goblin;
    }

    public Goblin merge(Goblin goblin) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        goblin = em.merge(goblin);
        tx.commit();
        em.close();
        return goblin;
    }

    public Goblin find(int id) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        Goblin goblin = em.find(Goblin.class, id);
        em.close();
        return goblin;
    }

    public void delete(Goblin goblin) {
        EntityManager em = EntityFactoryManager.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(goblin));
        tx.commit();
        em.close();
    }

    public List<Goblin> findAll() {
        EntityManager em = EntityFactoryManager.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Goblin> query = cb.createQuery(Goblin.class);
        query.from(Goblin.class);
        List<Goblin> goblins = em.createQuery(query).getResultList();
        em.close();
        return goblins;
    }

}
